//
// Created by michael on 18.09.2020.
//

#include "ConsoleInteractor.h"
using std::cin;
using std::cout;

ConsoleInteractor::~ConsoleInteractor() {
	CleanUp();
}

void ConsoleInteractor::Run() {
	std::string filename = "example.txt";

	std::ifstream fin(filename);
	std::ifstream& finRef = fin;


	try {
		_figsAmount = ConsoleInteractor::ReadInt(fin);
	}catch(const std::exception& exc){
		cout << exc.what();
		CleanUp();
	}

	_figures = new Shape* [_figsAmount];

	try {
		for (int counter = 0; counter < _figsAmount; counter++) {
			_figures[counter] = ParseFigureFromStream(finRef);
		}
	}
	catch (const std::exception& exc) {
		cout << exc.what();
		CleanUp();
	}

	ShowAllFigures(cout);

	int action = -1;

	ShowActionMenu(cout);

	while (true) {
		cout << ">>";
		try {
			action = ReadInt(cin);
		}
		catch (const std::exception& exc) {
			std::cerr << exc.what();
			return;
		}
		ProcessAction(action, cout);
		if (action == 0) {
			return;
		}
	}
}


void ConsoleInteractor::ShowActionMenu(std::ostream& stream) {
	cout << "1 - Get Area\n";
	cout << "2 - Get Gravity Center\n";
	cout << "3 - Rotate\n";
	cout << "4 - Move\n";
	cout << "5 - Get fig coords\n";
	cout << "6 - Intersecting\n";
	cout << "7 - Including\n";
	cout << "8 - Compare\n";
	cout << "0 - exit program\n\n";
}

void ConsoleInteractor::ProcessAction(int action, std::ostream& stream) {
	int figId1, figId2;

	if (action == 1) {

		cout << "Enter fig id: ";
		cin >> figId1;
		if (figId1 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		cout << "Area of " << typeid(*(_figures[figId1])).name() << " " << figId1 << " is " << _figures[figId1]->GetArea() << std::endl;

	} 
	else if (action == 2) {

		cout << "Enter fig id: ";
		cin >> figId1;
		if (figId1 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		Point& ref = _figures[figId1]->GetGravCenter();
		cout << "Grav center of " << typeid(*(_figures[figId1])).name() << " " << figId1 << " is (" << ref.x << ";" << ref.y << ")\n";
		
	}
	else if (action == 3) {
		cout << "Enter fig id: ";
		cin >> figId1;
		if (figId1 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		double degrees;
		cout << "Enter degrees: ";
		degrees = ReadDouble(cin);
		_figures[figId1]->Rotate(degrees);
		cout << "Rotated\n";
	}
	else if (action == 4) {
		cout << "Enter fig id: ";
		cin >> figId1;
		if (figId1 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		cout << "Enter offset like x y: ";
		double xOffset, yOffset;
		try {
			xOffset = ReadDouble(cin);
			yOffset = ReadDouble(cin);
		}
		catch (const std::exception& exc) {
			std::cerr << exc.what();
			return;
		}

		_figures[figId1]->Move(xOffset, yOffset);
		cout << "Moved\n";
	}
	else if (action == 5) {
		cout << "Enter fig id: ";
		cin >> figId1;
		if (figId1 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		ShowFigInfo(stream, figId1);
	}
	else if (action == 6) {
		cout << "Enter fig1 id and fig2 id like a b: ";
		cin >> figId1;
		cin >> figId2;
		if (figId1 >= _figsAmount || figId2 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		if (Operations::IsIntersect(_figures[figId1], _figures[figId2])) {
			cout << "figures intersect\n";
		}
		else {
			cout << "figures don`t intersect\n";
		}
	}
	else if (action == 7) {
		cout << "Enter fig id that`s included and fig id of placeholder like a b: ";
		cin >> figId1;
		cin >> figId2;
		if (figId1 >= _figsAmount || figId2 >= _figsAmount) {
			cout << "Wrong figId\n";
			return;
		}
		if (Operations::IsInclude(_figures[figId1], _figures[figId2])) {
			cout << "figure is included\n";
		}
		else {
			cout << "figure isn`t included\n";
		}

	}
	else if (action == 8) {
		cout << "Enter two figs ids: ";
		cin >> figId1 >> figId2;
		int res = Operations::Compare(_figures[figId1], _figures[figId2]);
		if (res == 0) {
			cout << "Fig 1 takes bigger area\n";
		}
		else if (res == 1) {
			cout << "Fig 2 takes bigger area\n";
		}
		else {
			cout << "Figs areas are same\n";
		}
	}
	else if (action == 0) {
		return;
	}
	else {
		cout << "wrong command number\n";
	}
 
}


Shape* ConsoleInteractor::ParseFigureFromStream(std::istream& stream) {
	char type;
	stream >> type;
	Shape* figure;
	Point* vertexes = nullptr;
	int vertexToSetAmount = 0;
	if (type == 'S') {
		vertexToSetAmount = 4;
		vertexes = new Point[vertexToSetAmount];
	}
	else if (type == 'H') {
		vertexToSetAmount = 6;
		vertexes = new Point[vertexToSetAmount];
	}

	
	std::string vertexesString;
	stream >> vertexesString;
	std::stringstream vertexesStream(vertexesString);
	
	char sep;
	bool open = false, close = false;

	int vInStringCounter = 0;

	while (vertexesStream >> sep) {
		if (sep == '(') {
			open = true;
		}
		else if (sep == ')') {
			close = true;
		}

		if (open && close) {
			vInStringCounter++;
			open = false;
			close = false;
		}
		else if (!open && close) {
			throw std::logic_error("illegal vertex format");
		}
	}

	if (vInStringCounter != vertexToSetAmount) {
		throw std::logic_error("too much or too few vertexes to init figure");
	}

	vertexesStream = std::stringstream(vertexesString);
	
	for (int counter = 0; counter < vertexToSetAmount; counter++) {
		vertexes[counter] = ConsoleInteractor::ReadVertex(vertexesStream);
	}

	figure = Shape::CreateShape(type, vertexes, vertexToSetAmount);

	return figure;
}

Point ConsoleInteractor::ReadVertex(std::istream& stream)
{
	Point vertex;
	char sep;

	try {
		stream >> sep;
		if (sep != '(') {
			throw std::logic_error("wrong vertex format");
		}
		vertex.x = ConsoleInteractor::ReadDouble(stream);

		stream >> sep;
		if (sep != ';') {
			throw std::logic_error("wrong vertex format");
		}
		vertex.y = ConsoleInteractor::ReadDouble(stream);

		stream >> sep;
		if (sep != ')') {
			throw std::logic_error("wrong vertex format");
		}
	}
	catch (const std::exception& exc) {
		throw std::exception(exc.what());
	}

	return vertex;
}

double ConsoleInteractor::ReadDouble(std::istream& stream)
{

	double res;

	if (!(stream >> res)) {
		throw std::logic_error("illegal double value");
	}

	return res;
}

int ConsoleInteractor::ReadInt(std::istream& stream)
{
	std::string input;
	stream >> input;

	int res;

	std::stringstream sin(input);

	if (!(sin >> res)) {
		throw std::logic_error("illegal int value");
	}

	return res;
}

void ConsoleInteractor::ShowFigInfo(std::ostream& stream, int figId) {
	stream << typeid(*(_figures[figId])).name() << std::endl;
	Point* vertexes = _figures[figId]->GetVertexes();
	for (int counter = 0; counter < _figures[figId]->GetVertexesAmount(); counter++) {
		stream << "(" << vertexes[counter].x << ";" << vertexes[counter].y << ") ";
	}
	stream << std::endl;
}

void ConsoleInteractor::ShowAllFigures(std::ostream& stream) {
	for (int counter = 0; counter < _figsAmount; counter++) {
		ShowFigInfo(stream, counter);
	}
}


void ConsoleInteractor::CleanUp() {
	for (int counter = 0; counter < _figsAmount; counter++) {
		delete _figures[counter];
	}
	delete[] _figures;
}
