//
// Created by michael on 18.09.2020.
//

#include "Square.h"

Square::Square(Point* points, int pointsAmount): Shape(points, pointsAmount) {
}

Shape *Square::ConstructSquare(Point* points, int pointsAmount)
{
    if (pointsAmount != 4) {
        throw ShapeExc("wrong points amount for square");
    }

    if (IfCorrectShape(points, pointsAmount)) {
        Shape* square = new Square(points, pointsAmount);
        return square;
    }
    else {
        throw ShapeExc("wrong square shape");
    }
}


double Square::GetArea() {
    if (_area == 0) {
        _area = pow(CalcSideLen(_vertexes[0], _vertexes[1]), 2);
    }
    return _area;
}

Point& Square::GetGravCenter() {
    _gravCenter.x = _vertexes[0].x + (_vertexes[2].x - _vertexes[0].x) / 2;
    _gravCenter.y = _vertexes[0].y + (_vertexes[2].y - _vertexes[0].y) / 2;
    return _gravCenter;
}




