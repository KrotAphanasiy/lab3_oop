//
// Created by michael on 18.09.2020.
//

#include "Hexagon.h"

Hexagon::Hexagon(Point* points, int pointsAmount): Shape(points, pointsAmount){

}

Shape *Hexagon::ConstructHexagon(Point* points, int pointsAmount) {
    if (pointsAmount != 6) {
        throw ShapeExc("wrong points amount for hexagon");
    }

    if (IfCorrectShape(points, pointsAmount)) {
        Shape* hexagon = new Hexagon(points, pointsAmount);
        return hexagon;
    }
    else {
        throw ShapeExc("wrong hexagon shape");
    }
}

double Hexagon::GetArea() {
    if (_area == 0) {
        _area = 3 * sqrt(3) / 2 * pow(CalcSideLen(_vertexes[0], _vertexes[1]), 2);
    }
    return _area;
}

Point& Hexagon::GetGravCenter() {
    _gravCenter.x = _vertexes[0].x + (_vertexes[3].x - _vertexes[0].x) / 2;
    _gravCenter.y = _vertexes[0].y + (_vertexes[3].y - _vertexes[0].y) / 2;
    return _gravCenter;
}
