//
// Created by michael on 19.09.2020.
//

#ifndef LAB3_OOP_SHAPEEXC_H
#define LAB3_OOP_SHAPEEXC_H

#include <exception>

class ShapeExc: public std::exception{
public:
    explicit ShapeExc(const char* error): _error(error){}
    const char* what() const noexcept override{
        return _error;
    }
private:
    const char* _error;
};

#endif LAB3_OOP_SHAPEEXC_H
