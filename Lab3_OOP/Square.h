//
// Created by michael on 18.09.2020.
//

#ifndef LAB3_OOP_SQUARE_H
#define LAB3_OOP_SQUARE_H
#include "Shape.h"

class Square : public Shape{
public:
    static Shape* ConstructSquare(Point* points, int pointsAmount);

    double GetArea() override;
    Point& GetGravCenter() override;

private:
    Square(Point* points, int pointsAmount);
};


#endif LAB3_OOP_SQUARE_H
