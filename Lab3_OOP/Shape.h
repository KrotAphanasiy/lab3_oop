//
// Created by michael on 18.09.2020.
//

#ifndef LAB3_OOP_SHAPE_H
#define LAB3_OOP_SHAPE_H
#include "ShapeExc.h"

struct Point{
    double x = 0, y = 0;
};

class Shape {
public:
    static Shape* CreateShape(char type, Point* points, int pointsAmount);

    virtual double GetArea() = 0;
    virtual Point& GetGravCenter() = 0;
    virtual void Rotate(int degrees);
    virtual void Move(double xOffset, double yOffset);

    Point* GetVertexes() const;
    int GetVertexesAmount() const;

    static double CalcSideLen(const Point& begPoint, const Point& endPoint);
    static double CalcScalarPr(const Point& begP1, const Point& endP1, const Point& begP2, const Point& endP2);


    virtual ~Shape();
protected:

    Shape() = default;
    Shape(Point *points, int pointsAmount);

    Point* _vertexes = nullptr;
    int _vertexAmount = 0;
    double _area = 0;
    Point _gravCenter;

    int id = -1;

    static bool IfCorrectShape(const Point* points, int vertexAmount);
    static bool CheckSidesLen(const Point* points, int vertexAmount);
    static bool CheckAngles(const Point* points, int vertexAmount);
    
    static bool DbsAreSame(double a, double b);    

    void CleanUp();
};


#endif LAB3_OOP_SHAPE_H
