//
// Created by michael on 18.09.2020.
//

#ifndef LAB3_OOP_OPERATIONS_H
#define LAB3_OOP_OPERATIONS_H
#include "Shape.h"

class Operations {
public:
    static bool IsIntersect(const Shape* intersecter, const Shape* intersected);
    static bool IsInclude(const Shape* included, const Shape* placeholder);
    static int Compare(Shape* fig1, Shape* fig2);

private:
    static double Det(double a, double b, double c, double d);
    static bool Between(double a, double b, double c);
    static bool IsIntersectLines(Point a, Point b, Point c, Point d);
    static bool IntersectOnAxis(double a, double b, double c, double d);
    static bool IsIncludePoint(const Point& point, const Shape* fig);
};


#endif LAB3_OOP_OPERATIONS_H
