//
// Created by michael on 18.09.2020.
//

#include "Operations.h"
#include <algorithm>
#include <cmath>

using std::swap;
using std::min;
using std::max;
using std::acos;

const double EPS = 1E-9;
const double Pi = 3.141592653589793238462643383279502884;

double Operations::Det(double a, double b, double c, double d) {
	return a * d - b * c;
}

bool Operations::Between(double a, double b, double c) {
	return min(a, b) <= c + EPS && c <= max(a, b) + EPS;
}

bool Operations::IntersectOnAxis(double a, double b, double c, double d) {
	if (a > b)  swap(a, b);
	if (c > d)  swap(c, d);
	return max(a, c) <= min(b, d);
}


bool Operations::IsIntersectLines(Point a, Point b, Point c, Point d) {
	double A1 = a.y - b.y, B1 = b.x - a.x, C1 = -A1 * a.x - B1 * a.y;
	double A2 = c.y - d.y, B2 = d.x - c.x, C2 = -A2 * c.x - B2 * c.y;
	double zn = Det(A1, B1, A2, B2);
	if (zn != 0) {
		double x = -Det(C1, B1, C2, B2) * 1. / zn;
		double y = -Det(A1, C1, A2, C2) * 1. / zn;
		return Between(a.x, b.x, x) && Between(a.y, b.y, y)
			&& Between(c.x, d.x, x) && Between(c.y, d.y, y);
	}
	else {
		return Det(A1, C1, A2, C2) == 0 && Det(B1, C1, B2, C2) == 0
			&& IntersectOnAxis(a.x, b.x, c.x, d.x)
			&& IntersectOnAxis(a.y, b.y, c.y, d.y);
	}
}

bool Operations::IsIntersect(const Shape* intersecter, const Shape* intersected) {
	Point* intersecterVertexes = intersecter->GetVertexes();
	int intersecterVertexAmount = intersecter->GetVertexesAmount();

	Point* intersectedVertexes = intersected->GetVertexes();
	int intersectedVertexAmount = intersected->GetVertexesAmount();

	bool result = false;

	for (int intersecterVCounter = 1; intersecterVCounter < intersecterVertexAmount - 1 && !result; intersecterVCounter++) {
		for (int intersectedVCounter = 1; intersectedVCounter < intersectedVertexAmount - 1 && !result; intersectedVCounter++) {
			if (IsIntersectLines(intersecterVertexes[intersecterVCounter - 1], intersecterVertexes[intersecterVCounter],
				intersectedVertexes[intersectedVCounter - 1], intersectedVertexes[intersectedVCounter])) {
				result = true;
			}
		}
	}

	if (IsIntersectLines(intersecterVertexes[intersecterVertexAmount - 1], intersecterVertexes[0],
		intersectedVertexes[intersectedVertexAmount - 1], intersectedVertexes[0])) {
		result = true;
	}

	return result;
}

bool Operations::IsIncludePoint(const Point& point, const Shape* fig)
{
	bool result = false;

	Point* figVertexes = fig->GetVertexes();
	int figVAmount = fig->GetVertexesAmount();
	double angleSum = 0;

	double scalar;
	double len1, len2;

	for (int counter = 0; counter < figVAmount; counter++) {
		scalar = Shape::CalcScalarPr(point, figVertexes[counter], point, figVertexes[counter + 1]);
		len1 = Shape::CalcSideLen(point, figVertexes[counter]);
		len2 = Shape::CalcSideLen(point, figVertexes[counter + 1]);
		angleSum += acos(scalar / (len1 * len2));
	}

	if (fabs(angleSum - 2 * Pi) < EPS) {
		result = true;
	}

	return result;
}

bool Operations::IsInclude(const Shape* included, const Shape* placeholder) {
	bool result = true;
	Point* includedVertexes = included->GetVertexes();
	int includedVAmount = included->GetVertexesAmount();

	for (int counter = 0; counter < includedVAmount && result; counter++) {
		if (!IsIncludePoint(includedVertexes[counter], placeholder)) {
			result = false;
		}
	}

	return result;
}

int Operations::Compare(Shape* fig1, Shape* fig2) {
	if (fig1->GetArea() > fig2->GetArea()) {
		return 0;
	}
	else if (fig1->GetArea() < fig2->GetArea()){
		return 1;
	}
	else {
		return -1;
	}
}
