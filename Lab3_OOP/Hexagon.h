//
// Created by michael on 18.09.2020.
//

#ifndef LAB3_OOP_HEXAGON_H
#define LAB3_OOP_HEXAGON_H
#include "Shape.h"

class Hexagon : public Shape {
public:
    static Shape* ConstructHexagon(Point* points, int pointsAmount);

    double GetArea() override;
    Point& GetGravCenter() override;

private:
    Hexagon(Point* points, int pointsAmount);
};


#endif LAB3_OOP_HEXAGON_H
