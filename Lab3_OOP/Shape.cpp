//
// Created by michael on 18.09.2020.
//

#include "Shape.h"
#include "Square.h"
#include "Hexagon.h"
#include <cmath>
using std::sin;
using std::cos;

const double pi = 3.14159265359;


bool Shape::DbsAreSame(double a, double b)
{
    return fabs(a - b) < 1e-24;
}

Shape::Shape(Point* points, int pointsAmount) :
    _vertexes(points),
    _vertexAmount(pointsAmount) {
}

void Shape::CleanUp() {
    delete[] _vertexes;
}

Shape::~Shape() {
    CleanUp();
}

Point* Shape::GetVertexes() const {
    return _vertexes;
}

int Shape::GetVertexesAmount() const {
    return _vertexAmount;
}

Shape *Shape::CreateShape(char type, Point* points, int pointsAmount) {
    Shape* shapePtr = nullptr;

    switch (type) {
        case 'S':
            try {
                shapePtr = Square::ConstructSquare(points, pointsAmount);
                break;
            }
            catch (const ShapeExc& exc) {
                throw exc;
            }
        case 'H':
            try {
                shapePtr = Hexagon::ConstructHexagon(points, pointsAmount);
                break;
            }
            catch (const ShapeExc& exc) {
                throw exc;
            }
        default:
            throw ShapeExc("wrong shape type " + type);
            break;
    }
    return shapePtr;
}

bool Shape::IfCorrectShape(const Point* points, int vertexAmount)
{
    bool result = true;

    if (!CheckSidesLen(points, vertexAmount) || !CheckAngles(points, vertexAmount)) {
        result = false;
    }

    return result;
}

bool Shape::CheckSidesLen(const Point* points, int vertexAmount)
{
    bool result = true;
    double prevLen = -1;

    for (int counter = 1; counter < vertexAmount; counter++) {
        double sideLen = CalcSideLen(points[counter - 1], points[counter]);
        if (prevLen < 0) {
            prevLen = sideLen;
        }
        else if (!DbsAreSame(sideLen, prevLen)) {
            result = false;
        }
    }

    return result;
}

bool Shape::CheckAngles(const Point* points, int vertexAmount) {
    bool result = true;
    double curCos = -2;
    double prevCos = -2;

    for (int counter = 1; counter < vertexAmount; counter++) {
        if (counter < vertexAmount - 1) {
            curCos = CalcScalarPr(points[counter-1], points[counter],  points[counter], points[counter+1]) / 
                (CalcSideLen(points[counter - 1], points[counter]) * CalcSideLen(points[counter], points[counter + 1]));
        }
        else {
            curCos = CalcScalarPr(points[counter - 1], points[counter], points[counter], points[0]) /
                (CalcSideLen(points[counter - 1], points[counter]) * CalcSideLen(points[counter], points[0]));
        }

        if (prevCos < -1) {
            prevCos = curCos;
        }
        else if (!DbsAreSame(prevCos, curCos)) {
            result = false;
        }
    }
    
    return result;
}

double Shape::CalcSideLen(const Point& begPoint, const Point& endPoint) {
    return sqrt(pow(endPoint.x - begPoint.x, 2) + pow(endPoint.y - begPoint.y, 2));
}

double Shape::CalcScalarPr(const Point& begP1, const Point& endP1, const Point& begP2, const Point& endP2) {
    return (endP1.x - begP1.x) * (endP2.x - begP2.x) + (endP1.y - begP1.y) * (endP2.y - begP2.y);
}

void Shape::Rotate(int degrees) {

    Point oldCoords;
    double angle = (degrees % 360)* pi / 180;

    for (int counter = 0; counter < _vertexAmount; counter++) {
        oldCoords = _vertexes[counter];
        _vertexes[counter].x = oldCoords.x * cos(angle) - oldCoords.y * sin(angle);
        _vertexes[counter].y = oldCoords.x * sin(angle) + oldCoords.y * cos(angle);
    }
}

void Shape::Move(double xOffset, double yOffset) {
    for (int counter = 0; counter < _vertexAmount; counter++) {
        _vertexes[counter].x += xOffset;
        _vertexes[counter].y += yOffset;
    }
}
