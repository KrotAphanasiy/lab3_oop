//
// Created by michael on 18.09.2020.
//

#ifndef LAB3_OOP_CONSOLEINTERACTOR_H
#define LAB3_OOP_CONSOLEINTERACTOR_H
#include "Shape.h"
#include "Operations.h"
#include <iostream>
#include <fstream>
#include <istream>
#include <sstream>

#include <typeinfo>

class ConsoleInteractor {
public:
	ConsoleInteractor() = default;

	void Run();

	~ConsoleInteractor();
private:

	static double ReadDouble(std::istream& stream);
	static int ReadInt(std::istream& stream);
	static Point ReadVertex(std::istream& stream);

	Shape** _figures = nullptr;
	int _figsAmount = 0;
	Shape* ParseFigureFromStream(std::istream& stream);
	void ShowFigInfo(std::ostream& stream, int figId);
	void ShowAllFigures(std::ostream& stream);
	void ShowActionMenu(std::ostream& stream);
	void ProcessAction(int action, std::ostream& stream = std::cout);
	
	void CleanUp();
};


#endif LAB3_OOP_CONSOLEINTERACTOR_H
